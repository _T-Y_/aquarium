#include "Reproduction.h"
#include "algae/Algue.h"
#include "fish/Poisson.h"

bool Reproduction::algae_reproduction(Algue *a){
    /*
    Reproductive function.
    If an alga has more than 10 HP, it divides its life into 2 and then returns true to create a new alga.
    I simulate the duplication of the algae.
    If it does not have 10 PV, I return false to indicate that the algae does not divide.
    */
    if(a->get_PV() >= 10){
        a->init_PV(a->get_PV() / 2);
        return true;
    }
    return false;  
}

Poisson *Reproduction::fish_reproduction(Poisson *p, Poisson *fish_random)
{
    /**
     * Function that manages the reproduction of all the fish in the list 
    */
    Poisson *young_fish = NULL;

    if (p->the_hungry_fish() == false)
    {
        //int fish_random = this->random_fish();
        //cout << "Poisson 1 : " << p->get_species() << ". Poisson 2 : " << fish_random->get_species() << endl;
        if (p->get_species() == fish_random->get_species())
        {
            int reproduction_age = 0;

            int sex = this->random_sex();
            char s;
            if (sex == 0)
            {
                s = 'M';
            }
            else if (sex == 1)
            {
                s = 'F';
            }
            if (p->get_age() >= reproduction_age && fish_random->get_age() >= reproduction_age)
            {
                if (p->get_reproduction() == "mono_gendered" || p->get_reproduction() == "hermaphrodite_with_age")
                {
                    // Comme vous et moi, ces poissons naissent mâle ou femelle et n'en changent plus.
                    // Ce poisson passe les 10 premiers tours de sa vie en tant que mâle et les 10 suivants en tant que femelle
                    if (p->get_sex() != fish_random->get_sex())
                    {
                        young_fish = new Poisson(0, 2, s, p->get_nutrition(), p->get_reproduction(), p->get_species());
                        //this->add_fish(p);
                    }
                }
                else
                { // opportunistic_hermaphrodite
                    //Ce poisson va rencontrer un membre de son espèce. Si ce membre est de même sexe que lui, notre poisson change de sexe pour pouvoir se reproduire.
                    if (p->get_sex() == fish_random->get_sex())
                    {
                        if (p->get_sex() == 'M')
                        {
                            p->set_sex('F');
                        }
                        else
                        {
                            p->set_sex('M');
                        }
                    }
                    young_fish = new Poisson(0, 2, s, p->get_nutrition(), p->get_reproduction(), p->get_species());
                    //this->add_fish(p);
                }
            }
        }
    }
    return young_fish;
}

int Reproduction::random_sex(){
    /**
     * Select a sex at random.
     * Return 0 or 1 for M or F
     */
    return rand() % 2;
}

void Reproduction::hermaphrodite(Poisson *p){
    /**
     * Function that manages the sex change of hermaphroditic fish
    */

    if(p->get_reproduction() == "hermaphrodite_with_age"){
        if(p->get_age() == 10){
            if(p->get_sex() == 'M'){
                p->set_sex('F');
            }else{
                p->set_sex('M');
            }
        }
    }
    
}