#ifndef DEF_BE_ALIVE
#define DEF_BE_ALIVE

#include "Reproduction.h"

    class Be_alive : public Reproduction{
        // Méthodes
        public:
        void aging();
        bool natural_death();
        int get_PV();
        int get_age();
        void set_PV(int PV);
        void init_PV(int PV);
        void hunger();
        bool is_dead();
    
        // Attributs
        protected:
        int PV = 0;
        int age = 0;
    };

#endif