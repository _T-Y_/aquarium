#include <iostream>
#include "Poisson.h"

using namespace std;

Poisson::Poisson(int age, int PV, char sex, std::string nutrition, std::string reproduction, std::string species){
    /*
    Constructor of the fish class
    */
    if(sex != 'M' && sex != 'F'){
        cout << "Mauvaise initialisation sex" << endl;
        exit(0);
    }
    if(nutrition != "carnivorous" && nutrition != "herbivorous"){
        cout << "Mauvaise initialisation nutrition" << endl;
        exit(0);
    }
    if(reproduction != "mono_gendered" && reproduction != "hermaphrodite_with_age" && reproduction != "opportunistic_hermaphrodite"){
        cout << "Mauvaise initialisation reproduction" << endl;
        exit(0);    
    }
    if(species != "sole" && species != "poisson_clown" && species != "bar" && species != "merou" && species != "carpe" && species != "thon"){
        cout << "Mauvaise initialisation species" << endl;
        exit(0);
    }

    this->sex = sex;
    this->nutrition = nutrition;
    this->reproduction = reproduction;
    this->species = species;
    this->age = age;
    this->PV = PV;
}

bool Poisson::the_hungry_fish(){
    /*
    Function that indicates when the fish is hungry
    */
    if(this->PV <= 5){
        return true;
    }else{
        return false;
    }
}

void Poisson::was_bitten(){
    /*
    Function that removes life points from the fish when it is bitten
    */
    this->PV -= 4;
}

char Poisson::get_sex(){
    /*
    Function which return the sex of the fish
    */
    return this->sex;
}

void Poisson::set_sex(char sex){
    /*
    Function which gives a sex to the fish
    */
    if(sex != 'M' || sex != 'F'){
        this->sex = sex;
    }
}

void Poisson::eat(){
    /*
    Function which gives a number of VP.
    This function checks if it is herbivorous or carnivorous.
    */
    if(this->nutrition == "herbivorous"){
        this->PV += 3;
    }
    this->PV += 5;
}

std::string Poisson::get_nutrition(){
    /*
    Return herbivorous or carnivorous
    */
    return this->nutrition;
}

std::string Poisson::get_reproduction(){
    /*
    Return F or M
    */
    return this->reproduction;
}

std::string Poisson::get_species(){
    /*
    Return the species of the fish. "carpe" or "thon" or "merou" or ... 
    */
    return this->species;
}

