#ifndef DEF_POISSON
#define DEF_POISSON

#include <string>
#include <iostream>
#include "../Be_alive.h"
//#include "../algae/Algue.h"

using namespace std;

    class Poisson : public Be_alive{
        //Méthode
        public:
        Poisson(int age, int PV, char sex, std::string nutrition, std::string reproduction, std::string species);
        bool the_hungry_fish();
        void was_bitten();
        char get_sex();
        void set_sex(char sex);
        void eat();
        std::string get_nutrition();
        std::string get_reproduction();
        std::string get_species();

        //Attribut
        private:
        char sex;
        std::string nutrition;
        std::string reproduction;
        std::string species;
    };

#endif