#ifndef DEF_ALGUE
#define DEF_ALGUE

#include <iostream>
#include "../Be_alive.h"

    class Algue : public Be_alive{
        //Méthodes
        public:
        void was_bitten();
        Algue(int age_init, int PV_init);
        void get_fat();

        //Attribut
        private:
    };

#endif