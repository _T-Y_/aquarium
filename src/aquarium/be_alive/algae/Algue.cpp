#include "Algue.h"

using namespace std;

Algue::Algue(int age_init, int PV_init){
    /*
    Constructeur de la classe Algue
    */
    this->age = age_init;
    this->PV = PV_init;
}

void Algue::was_bitten(){
    /*
    Function that manages the bite of a fish
    */
    this->PV -= 2;
}

void Algue::get_fat(){
    /*
    Function that adds +1 HP to the seaweed.
    The seaweed grows bigger as it ages.
    This is represented by adding + 1PV
    */
    this->PV += 1;
}