#include "Be_alive.h"

using namespace std;

void Be_alive::aging(){
    /*
    Aging function.
    +1 in age
    */
    this->age += 1;
}

bool Be_alive::natural_death(){
    /*
    If the algae is 20 years old, the function returns true.
    Otherwise, it returns false.
    Living beings live up to 20 years.
    When the function returns true, the element is removed from the list.
    */
    if(this->age > 20){
        return true;
    }else{
        return false;
    }
}

int Be_alive::get_PV(){
    /*
    Function that returns the amount of health
    */
    return this->PV;
}

int Be_alive::get_age(){
    /*
    Function that returns the age
    */
    return this->age;
}

void Be_alive::set_PV(int PV){
    /*
    Function that modifies the number of PV
    */
    this->PV += PV;
}

void Be_alive::init_PV(int PV){
    /*
    Function that init the number of PV
    */
    this->PV = PV;
}

void Be_alive::hunger(){
    /*
    Each turn, the living being more and more hungry.
    He therefore loses 1 PV
    */
    this->PV -= 1;
}

bool Be_alive::is_dead(){
    /*
    If the being alive at 20 or if he has no more health, he is dead.
    */
    if(this->PV <= 0 || this->age >= 20)
        return true;
    return false;
}