#ifndef DEF_REPRODUCTION
#define DEF_REPRODUCTION

//#include "algae/Algue.h"
//#include "fish/Poisson.h"
//https://openclassrooms.com/forum/sujet/expected-class-name-before-token
//!!!!!inclusion circulaire!!!!!!

    class Poisson;
    class Algue;

    class Reproduction{
        // Méthodes
        public:
        bool algae_reproduction(Algue *a);
        Poisson* fish_reproduction(Poisson *p, Poisson *fish_random);
        int random_sex();
        void hermaphrodite(Poisson *p);

        // Attributs
        private:

    };

#endif