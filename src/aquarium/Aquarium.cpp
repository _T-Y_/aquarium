#include <iostream>
#include <time.h>
#include <list>
#include "Aquarium.h"
#include "be_alive/algae/Algue.h"
#include "be_alive/fish/Poisson.h"
#include "display/Display.h"

using namespace std;

float Aquarium::new_time(float time){
    /*
    Return the new time of the simulation
    */
    return (time + 1); 
}

void Aquarium::add_fish(Poisson *P){
    /*
    Add a new fish to the list
    */
    this->fish_list.push_back(P);
}

void Aquarium::add_algae(Algue *A){
    /*
    Add a new algae to the list
    */
    this->algae_list.push_back(A);
}

void Aquarium::fish_initialization(int nb){
    /*
    Function that creates fish, is the additions to the list.
    This an initialization function.
    */
    
    // Herbivore

    for(int i = 0; i < 10 ; i++){
        Poisson *p1 = new Poisson(0,10,'M',"herbivorous","hermaphrodite_with_age","bar");
        this->add_fish(p1);
    }
    for(int i = 0; i < 10 ; i++){
        Poisson *p2 = new Poisson(0,10,'F',"herbivorous","hermaphrodite_with_age","bar");
        this->add_fish(p2);
    }

    for(int i = 0; i < 10 ; i++){
        Poisson *p3 = new Poisson(0,10,'M',"herbivorous","mono_gendered","carpe");
        this->add_fish(p3);
    }
    for(int i = 0; i < 10 ; i++){
        Poisson *p4 = new Poisson(0,10,'F',"herbivorous","mono_gendered","carpe");
        this->add_fish(p4);
    }

    for(int i = 0; i < 5 ; i++){
        Poisson *p9 = new Poisson(0,10,'M',"herbivorous","opportunistic_hermaphrodite","sole");
        this->add_fish(p9);
    }
    for(int i = 0; i < 5 ; i++){
        Poisson *p10 = new Poisson(0,10,'F',"herbivorous","opportunistic_hermaphrodite","sole");
        this->add_fish(p10);
    }


    // Carnivore

    for(int i = 0; i < 15 ; i++){
        Poisson *p5 = new Poisson(0,10,'M',"carnivorous","hermaphrodite_with_age","merou");
        this->add_fish(p5);
    }
    for(int i = 0; i < 7 ; i++){
        Poisson *p6 = new Poisson(0,10,'F',"carnivorous","hermaphrodite_with_age","merou");
        this->add_fish(p6);
    }

    for(int i = 0; i < 6 ; i++){
        Poisson *p7 = new Poisson(0,10,'M',"carnivorous","opportunistic_hermaphrodite","poisson_clown");
        this->add_fish(p7);
    }
    for(int i = 0; i < 5 ; i++){
        Poisson *p8 = new Poisson(0,10,'F',"carnivorous","opportunistic_hermaphrodite","poisson_clown");
        this->add_fish(p8);
    }

    for(int i = 0; i < 15 ; i++){
        Poisson *p11 = new Poisson(0,10,'M',"carnivorous","mono_gendered","thon");
        this->add_fish(p11);
    }
    for(int i = 0; i < 12 ; i++){
        Poisson *p12 = new Poisson(0,10,'F',"carnivorous","mono_gendered","thon");
        this->add_fish(p12);
    }
}

void Aquarium::algae_initialization(int nb){
    /*
    Function that creates algae, is the additions to the list.
    This an initialization function.
    */
    for(int i = 0; i < nb; i++){
        Algue *algae = new Algue(0,10);
        this->add_algae(algae);
    }
}

void Aquarium::more_and_more_hungry(){
    /*
    Function that calls the hunger class method.
    The fish are increasingly hungry
    */
    int lenght = this->fish_list.size();
    for(int i = 0; i < lenght; i++){
        this->fish_list[i]->hunger();
    }
}

void Aquarium::aging(){
    /*
    Living things grow old
    */
    int lenght = this->fish_list.size();
    for(int i = 0; i < lenght; i++)
        this->fish_list[i]->aging();

    lenght = this->algae_list.size();
    for(int i = 0; i < lenght; i++){
        this->algae_list[i]->aging();
        this->algae_list[i]->get_fat();
    }
}

void Aquarium::death_check(){
    /*
    Remove dead items from lists
    */
    int i = 0;
    bool sup = false;
    int lenght = this->fish_list.size();

    while(i <= lenght-1){
        for(i = 0; i < lenght; i++){
            if(this->fish_list[i]->is_dead()){
                sup = true;
                break;
            }
        }
        if(sup == true){
            this->fish_list.erase(this->fish_list.begin()+i);
            sup = false;
        }
        lenght = this->fish_list.size();
    }

    i = 0;
    sup = false;
    lenght = this->algae_list.size();

    while(i <= lenght-1){
        for(i = 0; i < lenght; i++){
            if(this->algae_list[i]->is_dead()){
                sup = true;
                break;
            }
        }
        if(sup == true){
            this->algae_list.erase(this->algae_list.begin()+i);
            sup = false;
        }
        lenght = this->algae_list.size();
    }
}

int Aquarium::random_fish(){
    /**
     * Select a fish at random.
     * Return the position of the fish on the list
    */
    return rand() % this->fish_list.size();
}

int Aquarium::random_algae(){
    /**
     * Select a algae at random.
     * Return the position of the fish on the list
    */
    return rand() % this->algae_list.size();
}

void Aquarium::hunt(){
    /*
    Function that manages the hunting of all fish
    */

    int lenght = this->fish_list.size();
    
    for(int i = 0; i < lenght; i++){
        if(this->fish_list[i]->get_PV() > 0 && this->fish_list[i]->the_hungry_fish()){
            if(this->fish_list[i]->get_species() == "merou" || this->fish_list[i]->get_species() == "thon" || this->fish_list[i]->get_species() == "poisson_clown"){
                int hunt_fish = this->random_fish();
                //cout << this->fish_list[i]->get_species() << " "<< this->fish_list[i]->get_age() << " " << this->fish_list[hunt_fish]->get_species() << " " << this->fish_list[hunt_fish]->get_age() << endl;
                
                if((hunt_fish != i) && this->fish_list[i]->get_species() != this->fish_list[hunt_fish]->get_species()){
                    //cout << "MIAM" << endl << endl;
                    this->fish_list[i]->eat();
                    this->fish_list[hunt_fish]->was_bitten();
                }
            }else{
                if(this->algae_list.size() > 0){
                    int hunt_algae = this->random_algae();
                    this->fish_list[i]->eat();
                    this->algae_list[hunt_algae]->was_bitten();
                }
            }
        }
    }
}

void Aquarium::passing_time(){
    //cout << "Hungry" << endl;
    this->more_and_more_hungry();
    //cout << "Aging" << endl;    
    this->aging();
    //cout << "Death check" << endl;
    this->death_check();    
}

void Aquarium::reproduction(){
    /**
     * Function that manages the reproduction of algae and fish
     */
     
    // Algae part

    bool clone = false;
    int size = this->algae_list.size();

    for(int i = 0; i < size; i++){
        if(this->algae_list.size() > 5000)
            break;

        clone = this->algae_list[i]->algae_reproduction(this->algae_list[i]);
        
        if(clone == true){
            Algue *a = new Algue(0,this->algae_list[i]->get_PV());
            this->add_algae(a);
        }

        size = this->algae_list.size();
    }

    // Fish part

    int lenght = this->fish_list.size();
    int fish_random = 0;

    for(int i = 0; i < lenght; i++){
        if(this->fish_list.size() > 1000){
            break;
        }
        
        this->fish_list[i]->hermaphrodite(this->fish_list[i]);
        
        fish_random = this->random_fish();
        if(i != fish_random){
            //Poisson *fish = new Poisson(this->fish_list[fish_random]->get_age(),this->fish_list[fish_random]->get_age(),this->fish_list[fish_random]->get_sex(),this->fish_list[fish_random]->get_nutrition(),this->fish_list[fish_random]->get_reproduction(),this->fish_list[fish_random]->get_species());
            Poisson *fish = this->fish_list[fish_random];

            Poisson *p = this->fish_list[i]->fish_reproduction(this->fish_list[i],fish);
            if(p)
                this->add_fish(p);
        }
    }
}

void Aquarium::simulation(int duration){
    /*
    Info sur les listes 
    https://www.progmatique.fr/article-11-Cpp-classe-list-stl.html
     */

    Display disp = Display();     
    this->algae_initialization(1000);
    this->fish_initialization(2);

    float time = 0;

    while(time < duration && this->fish_list.size() > 0){
        disp.affichage_count(time,this->fish_list,this->algae_list);
        this->hunt();
        time = this->new_time(time);
        this->passing_time();
        this->reproduction();
    }
}