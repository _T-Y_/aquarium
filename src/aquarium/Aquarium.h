#ifndef DEF_AQUARIUM
#define DEF_AQUARIUM

#include <string.h>
#include <vector>
#include <variant>
#include "be_alive/algae/Algue.h"
#include "be_alive/fish/Poisson.h"

using namespace std;

    class Aquarium{
        //Méthode
        public:

        float new_time(float time);
        void add_fish(Poisson *P);
        void add_algae(Algue *A);       
        void more_and_more_hungry();
        void aging();
        void death_check();
        void fish_initialization(int nb);
        void algae_initialization(int nb);
        int random_fish();
        int random_algae();
        void simulation(int duration);
        void hunt();
        void hunt_action(Poisson *fish1, Poisson *fish2);
        void affichage(int tour);
        void affichage_count(int tour);
        void passing_time();
        void reproduction();

        //Attribut 
        private:

        int time;
        std::vector<Algue *> algae_list;
        std::vector<Poisson *> fish_list; 
    };

#endif