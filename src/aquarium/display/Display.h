#ifndef DEF_DISPLAY
#define DEF_DISPLAY

#include <vector>
using namespace std;

    class Poisson;
    class Algue;
    
    class Display{
        //Méthodes
        public:
        void affichage(int tour,std::vector<Poisson *> fish_list,std::vector<Algue *> algae_list);
        void affichage_count(int tour,std::vector<Poisson *> fish_list,std::vector<Algue *> algae_list);

        // Attributs
        private:
    };

#endif