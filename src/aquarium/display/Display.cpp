#include "Display.h"
#include "../be_alive/algae/Algue.h"
#include "../be_alive/fish/Poisson.h"

void Display::affichage(int tour, std::vector<Poisson *> fish_list,std::vector<Algue *> algae_list){
    /**
     * Shows all information about living things
    */

    cout << "Aquarium tour : " << tour << endl;
    cout << "Fish" << endl;

    int lenght = fish_list.size();

    for(int i = 0; i < lenght; i++){
        cout << "Espece : " << fish_list[i]->get_species() << ". PV : " << fish_list[i]->get_PV() << ". Sex : " << fish_list[i]->get_sex() << ". Age : " << fish_list[i]->get_age() << ". Nutrition : " << fish_list[i]->get_nutrition() << ". Reproduction : " << fish_list[i]->get_reproduction() << endl;
    }

    lenght = algae_list.size();
    cout <<"Algae : "<< lenght <<endl;
    /*
    for(int i=0; i < lenght; i++){
        cout << "Algue " << i << " age : " << algae_list[i]->get_age() << ". PV : " << algae_list[i]->get_PV() << endl;
    }*/
}

void Display::affichage_count(int tour, std::vector<Poisson *> fish_list,std::vector<Algue *> algae_list){
    /**
     * Displays the counters of each species
    */

    int lenght = fish_list.size();
    
    int merou_count = 0;
    int bar_count = 0;
    int poisson_clown_count = 0;
    int carpe_count = 0;
    int thon_count = 0;
    int sole_count = 0;

    for(int i = 0; i < lenght; i++){
        if(fish_list[i]->get_species() == "merou"){
            merou_count += 1;
        }
        if(fish_list[i]->get_species() == "carpe"){
            carpe_count += 1;
        }
        if(fish_list[i]->get_species() == "bar"){
            bar_count += 1;
        }
        if(fish_list[i]->get_species() == "sole"){
            sole_count += 1;
        }
        if(fish_list[i]->get_species() == "thon"){
            thon_count += 1;
        }
        if(fish_list[i]->get_species() == "poisson_clown"){
            poisson_clown_count += 1;
        }
    }
    cout << "Tour : " << tour << endl << endl;

    cout << "Algue         : " << algae_list.size() << endl;
    cout << "Merou         : " << merou_count << endl;
    cout << "Carpe         : " << carpe_count << endl;
    cout << "Bar           : " << bar_count << endl;
    cout << "Sole          : " << sole_count << endl;
    cout << "Thon          : " << thon_count << endl;
    cout << "Poisson clown : " << poisson_clown_count << endl << endl << endl;
}