#include <iostream>
#include "aquarium/Aquarium.h"

using namespace std;

int main()
{
    cout << "AQUARIUM" << endl;

    Aquarium aqua;
    aqua.simulation(70);

    return 0;
}