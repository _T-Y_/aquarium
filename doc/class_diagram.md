```mermaid
classDiagram
    Aquarium o-- Fish
    Aquarium o-- Algae
    Aquarium o-- Display

    Reproduction <|-- Be_alive
    Be_alive <|-- Fish
    Be_alive <|-- Algae
    
    class Aquarium{
        -int time
        -vector<Algue *> algae_list
        -vector<Poisson *> fish_list
        +float_new_time(float time)
        +void_add_fish(Poisson *P);
        +void_add_algae(Algue *A);       
        +void_more_and_more_hungry();
        +void_aging();
        +void_death_check();
        +void_fish_initialization(int_nb);
        +void_algae_initialization(int_nb);
        +int_random_fish();
        +int_random_algae();
        +void_simulation(int_duration);
        +void_hunt();
        +void_hunt_action(Poisson *fish1, Poisson *fish2);
        +void_affichage(int_tour);
        +void_affichage_count(int_tour);
        +void_algae_reproduction();
        +void_fish_reproduction();
        +int_random_sex();
        +void_hermaphrodite();
    }
    
    class Be_alive{
        -int_PV;
        -int_age;
        +void_aging();
        +bool_natural_death();
        +int_get_PV();
        +int_get_age();
        +void_set_PV(int_PV);
        +void_init_PV(int_PV);
        +void_hunger();
        +bool_is_dead();
    }

    class Fish{
        -char_sex;
        -std::string_nutrition;
        -std::string_reproduction;
        -std::string_species;
        +Poisson(int_age, int_PV, char_sex, std::string_nutrition, std::string_reproduction, std::string_species);
        +bool_the_hungry_fish();
        +void_was_bitten();
        +char_get_sex();
        +void_set_sex(char_sex);
        +void_eat();
        +std::string_get_nutrition();
        +std::string_get_reproduction();
        +std::string_get_species();
    }

    class Algae{
        +void_was_bitten();
        +Algue(int_age_init, int_PV_init);
        +void_get_fat();
    }

    class Reproduction{
        +bool_algae_reproduction(Algue *a);
        +Poisson*_fish_reproduction(Poisson *p, Poisson *fish_random);
        +int_random_sex();
        +void_hermaphrodite(Poisson *p);
    }
    class Display{
        +void_affichage(int_tour,std::vector<Poisson *> fish_list,std::vector<Algue *> algae_list);
        +void_affichage_count(int_tour,std::vector<Poisson *> fish_list,std::vector<Algue *> algae_list);
    }
```