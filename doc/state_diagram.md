```mermaid
graph TD
    A[Algae initialisation] -->|Algae ready| B(Fish initialisation)
    B --> |Fish ready| C(Display)
    C --> |Display done| D(hunt)
    D --> |Hunt done| E(New time of the simulation)
    E --> |New time done| F(The fish are hungry)
    F --> |-1 PV| G(Aging)
    G --> |+1 year| H(Death check)
    H --> |Living things that no longer have life or are 20 years old are dead| I(Hermaphrodite)
    I --> |Hermaphrodite fish that are 10 years old change sex| J(Algae reproduction)
    J --> |Done| K(Fish reproduction)
    K --> |Done| L{Simulation time ends? Fish population equal to 0?} 
    L --> |No| C
    L --> |Yes| M(End of the simulation)
```