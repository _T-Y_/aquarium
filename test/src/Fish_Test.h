#ifndef DEF_FISH_TEST
#define DEF_FISH_TEST

#include <iostream>
#include <memory>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>

#include "../../src/aquarium/be_alive/fish/Poisson.h"

    class Fish_Test : public CppUnit::TestCase{
        CPPUNIT_TEST_SUITE( Fish_Test );
        CPPUNIT_TEST( test_Poisson ) ;
        CPPUNIT_TEST( test_the_hungry_fish );
        CPPUNIT_TEST( test_was_bitten );
        CPPUNIT_TEST( test_get_sex );
        CPPUNIT_TEST( test_set_sex );
        CPPUNIT_TEST( test_eat );
        CPPUNIT_TEST_SUITE_END();

        // Méthodes
        public:
        void setUp();
        void tearDown();
        void test_Poisson();
        void test_was_bitten();
        void test_the_hungry_fish();
        void test_get_sex();
        void test_set_sex();
        void test_eat();

        // Attributs
        private:
        Poisson *p;
    };

#endif