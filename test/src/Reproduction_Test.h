#ifndef DEF_REPRODUCTION_TEST
#define DEF_REPRODUCTION_TEST

#include <iostream>
#include <memory>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>

#include "../../src/aquarium/be_alive/fish/Poisson.h"
#include "../../src/aquarium/be_alive/algae/Algue.h"

    class Reproduction_Test : public CppUnit::TestCase{
        CPPUNIT_TEST_SUITE( Reproduction_Test );
        CPPUNIT_TEST( test_algae_reproduction );
        CPPUNIT_TEST( test_fish_reproduction );
        CPPUNIT_TEST( test_hermaphrodite );
        CPPUNIT_TEST_SUITE_END();
        
        // Méthodes
        public:
        void setUp();
        void tearDown();
        void test_algae_reproduction();
        void test_fish_reproduction();
        void test_hermaphrodite();

        // Attributs
        private:
        Algue *a1;
        Poisson *p1;
        Poisson *p2;
        Poisson *p3;
        Poisson *p4;
        Poisson *p5;
    };

#endif