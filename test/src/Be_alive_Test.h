#ifndef DEF_BE_ALIVE_TEST
#define DEF_BE_ALIVE_TEST

#include <iostream>
#include <memory>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include "../../src/aquarium/be_alive/Be_alive.h"

    class Be_alive_Test : public CppUnit::TestCase{  
        CPPUNIT_TEST_SUITE( Be_alive_Test );
        CPPUNIT_TEST( test_PV ) ;
        CPPUNIT_TEST( test_age ) ;
        CPPUNIT_TEST( test_aging ) ;
        CPPUNIT_TEST( test_hunger ) ;
        CPPUNIT_TEST( test_is_dead_false ) ;
        CPPUNIT_TEST( test_is_dead_PV_true ) ;
        CPPUNIT_TEST( test_natural_death_false ) ;
        CPPUNIT_TEST( test_natural_death_true ) ;
        CPPUNIT_TEST( test_is_dead_age_true ) ;
        CPPUNIT_TEST_SUITE_END();

        // Méthodes
        public:
        void setUp();
        void tearDown();
        void test_PV();
        void test_age();
        void test_aging();
        void test_hunger();
        void test_is_dead_false();
        void test_is_dead_PV_true();
        void test_natural_death_false();
        void test_natural_death_true();
        void test_is_dead_age_true();

        // Atributs
        private:  
        int PV_test = 10;
        Be_alive *a;
    };

#endif