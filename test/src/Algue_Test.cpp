#include "Algue_Test.h"
#include "../../src/aquarium/be_alive/algae/Algue.h"

// Enregistrement des différents cas de tests
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION( Algue_Test, "Algue_Test" );

void Algue_Test::setUp(){
    // Initialisation pour les tests
    this->a1 = new Algue(10,10);
    this->a2 = new Algue(9,9);
}

void Algue_Test::tearDown(){
    delete this->a1;
    delete this->a2;
}

void Algue_Test::test_Algue1(){
    CPPUNIT_ASSERT(this->a1->get_PV() == 10);
    CPPUNIT_ASSERT(this->a1->get_age() == 10);
}

void Algue_Test::test_Algue2(){
    CPPUNIT_ASSERT(this->a2->get_PV() == 9);
    CPPUNIT_ASSERT(this->a2->get_age() == 9);
}

void Algue_Test::test_Reproduction(){
    int life = this->a1->get_PV();
    bool var = this->a1->algae_reproduction(this->a1);

    CPPUNIT_ASSERT(var == true);
    CPPUNIT_ASSERT(this->a1->get_PV() == life/2);

    var = this->a1->algae_reproduction(this->a1);

    CPPUNIT_ASSERT(var == false);
}

void Algue_Test::test_Was_bitten(){
    int life = this->a2->get_PV();
    this->a2->was_bitten();

    CPPUNIT_ASSERT(this->a2->get_PV() == life - 2);
}

void Algue_Test::test_get_fat(){
    int life = this->a2->get_PV();
    this->a2->get_fat();

    CPPUNIT_ASSERT(this->a2->get_PV() == life + 1);
}