#include "Reproduction_Test.h"

// Enregistrement des différents cas de tests
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION( Reproduction_Test, "Reproduction_Test" );

void Reproduction_Test::setUp(){
    // Initialisation pour les tests
    this->a1 = new Algue(1,10);
    this->p1 = new Poisson(1,10,'M',"carnivorous","mono_gendered","thon");
    this->p2 = new Poisson(1,10,'F',"carnivorous","mono_gendered","thon");
    this->p3 = new Poisson(1,10,'M',"carnivorous","opportunistic_hermaphrodite","poisson_clown");
    this->p4 = new Poisson(1,10,'M',"carnivorous","opportunistic_hermaphrodite","poisson_clown");
    this->p5 = new Poisson(1,10,'M',"herbivorous","hermaphrodite_with_age","carpe");
}

void Reproduction_Test::tearDown(){
    delete this->a1;
    delete this->p1;
    delete this->p2;
    delete this->p3;
    delete this->p4;
    delete this->p5;
}

void Reproduction_Test::test_algae_reproduction(){
    CPPUNIT_ASSERT( this->a1->get_PV() == 10 );
    bool var = this->a1->algae_reproduction(this->a1);
    CPPUNIT_ASSERT( var == true );
    CPPUNIT_ASSERT( this->a1->get_PV() == 5 );
}

void Reproduction_Test::test_fish_reproduction(){
    Poisson *young_fish = this->p1->fish_reproduction(this->p2,this->p1);

    CPPUNIT_ASSERT( young_fish->get_age() == 0 );
    CPPUNIT_ASSERT( young_fish->get_nutrition() == this->p1->get_nutrition());
    CPPUNIT_ASSERT( young_fish->get_PV() == 2 );
    CPPUNIT_ASSERT( young_fish->get_reproduction() == this->p1->get_reproduction() );
    CPPUNIT_ASSERT( young_fish->get_species() == this->p1->get_species() );

    Poisson *young_fish2 = this->p3->fish_reproduction(this->p3,this->p4);
    
    CPPUNIT_ASSERT( young_fish2->get_age() == 0 );
    CPPUNIT_ASSERT( young_fish2->get_nutrition() == this->p3->get_nutrition());
    CPPUNIT_ASSERT( young_fish2->get_PV() == 2 );
    CPPUNIT_ASSERT( young_fish2->get_reproduction() == this->p3->get_reproduction() );
    CPPUNIT_ASSERT( young_fish2->get_species() == this->p3->get_species() );

    CPPUNIT_ASSERT( this->p3->get_sex() == 'F' );

    delete young_fish;
    delete young_fish2;
}

void Reproduction_Test::test_hermaphrodite(){
    char sex = this->p5->get_sex();
    this->p5->hermaphrodite(this->p5);
    CPPUNIT_ASSERT( sex == this->p5->get_sex() );

    while(this->p5->get_age() < 10){
        this->p5->aging();
    }

    this->p5->hermaphrodite(this->p5);
    CPPUNIT_ASSERT( sex != this->p5->get_sex() );
}