#ifndef DEF_ALGUE_TEST
#define DEF_ALGUE_TEST

#include <memory>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
//#include "../../src/aquarium/be_alive/algae/Algue.h"

    class Algue;

    class Algue_Test : public CppUnit::TestCase{  
        CPPUNIT_TEST_SUITE( Algue_Test );
        CPPUNIT_TEST( test_Algue1 ) ;
        CPPUNIT_TEST( test_Algue2 ) ;
        CPPUNIT_TEST( test_Reproduction ) ;
        CPPUNIT_TEST( test_Was_bitten );
        CPPUNIT_TEST( test_get_fat ) ;
        CPPUNIT_TEST_SUITE_END();

        // Méthodes
        public:
        void setUp();
        void tearDown();
        void test_Algue1();
        void test_Algue2();
        void test_Reproduction();
        void test_Was_bitten();
        void test_get_fat();

        // Atributs
        private:
        Algue *a1;
        Algue *a2;     
    };

#endif