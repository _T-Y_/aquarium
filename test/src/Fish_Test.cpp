#include "Fish_Test.h"

// Enregistrement des différents cas de tests
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION( Fish_Test, "Fish_Test" );

void Fish_Test::setUp(){
    // Initialisation pour les tests
    this->p = new Poisson(1,10,'M',"carnivorous","mono_gendered","thon");
}

void Fish_Test::tearDown(){
    delete this->p;
}

void Fish_Test::test_Poisson(){
    CPPUNIT_ASSERT(this->p->get_age() == 1);
    CPPUNIT_ASSERT(this->p->get_PV() == 10);
    CPPUNIT_ASSERT(this->p->get_reproduction() == "mono_gendered");
    CPPUNIT_ASSERT(this->p->get_sex() == 'M');
    CPPUNIT_ASSERT(this->p->get_species() == "thon");
    CPPUNIT_ASSERT(this->p->get_nutrition() == "carnivorous");
}

void Fish_Test::test_was_bitten(){
    CPPUNIT_ASSERT( this->p->get_PV() == 10);
    this->p->was_bitten();
    CPPUNIT_ASSERT( this->p->get_PV() == 6);
    this->p->was_bitten();
    CPPUNIT_ASSERT( this->p->get_PV() == 2);
}

void Fish_Test::test_the_hungry_fish(){
    CPPUNIT_ASSERT( this->p->get_PV() == 10);
    CPPUNIT_ASSERT( this->p->the_hungry_fish() == false);
    this->p->was_bitten(); // PV = 6
    CPPUNIT_ASSERT( this->p->the_hungry_fish() == false);
    this->p->was_bitten(); // PV = 2
    CPPUNIT_ASSERT( this->p->the_hungry_fish() == true);
}

void Fish_Test::test_get_sex(){
    CPPUNIT_ASSERT( this->p->get_sex() == 'M' );
}

void Fish_Test::test_set_sex(){
    this->p->set_sex('F');
    CPPUNIT_ASSERT( this->p->get_sex() == 'F' );
    this->p->set_sex('M');
    CPPUNIT_ASSERT( this->p->get_sex() == 'M' );
}

void Fish_Test::test_eat(){
    CPPUNIT_ASSERT( this->p->get_PV() == 10);
    this->p->eat();
    CPPUNIT_ASSERT( this->p->get_PV() == 15 );
}