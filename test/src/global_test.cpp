#include "Algue_Test.h"
#include "Be_alive_Test.h"
#include "Fish_Test.h"
#include "Reproduction_Test.h"

CppUnit::Test *suite(){
  CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();

  registry.registerFactory(&CppUnit::TestFactoryRegistry::getRegistry( "Algue_Test" ) );
  registry.registerFactory(&CppUnit::TestFactoryRegistry::getRegistry( "Be_alive_Test" ) );
  registry.registerFactory(&CppUnit::TestFactoryRegistry::getRegistry( "Fish_Test" ) );
  registry.registerFactory(&CppUnit::TestFactoryRegistry::getRegistry( "Reproduction_Test" ) );
  
  return registry.makeTest();
}


int main( int argc, char* argv[] ){
  // if command line contains "-selftest" then this is the post build check
  // => the output must be in the compiler error format.
  bool selfTest = (argc > 1)  && (std::string("-selftest") == argv[1]);

  CppUnit::TextUi::TestRunner runner;
  runner.addTest( suite() );   // Add the top suite to the test runner

  if ( selfTest ){ 
    // Change the default outputter to a compiler error format outputter
    // The test runner owns the new outputter.
    runner.setOutputter( CppUnit::CompilerOutputter::defaultOutputter(&runner.result(), std::cerr ) ); 
  }

  // Run the test.
  bool wasSucessful = runner.run( "" );

  // Return error code 1 if any tests failed.
  return wasSucessful ? 0 : 1;
}