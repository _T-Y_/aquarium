#include "Be_alive_Test.h"

// Enregistrement des différents cas de tests
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION( Be_alive_Test, "Be_alive_Test" );

void Be_alive_Test::setUp(){
    // Initialisation pour les tests
    this->a = new Be_alive();
    a->set_PV(this->PV_test);
}

void Be_alive_Test::tearDown(){
    delete this->a;
}

void Be_alive_Test::test_PV(){
    CPPUNIT_ASSERT(a->get_PV() == this->PV_test);
}

void Be_alive_Test::test_age(){
    CPPUNIT_ASSERT(a->get_age() == 0);
}

void Be_alive_Test::test_aging(){
    a->aging();

    CPPUNIT_ASSERT(a->get_age() == 1);
}

void Be_alive_Test::test_hunger(){
    int life = a->get_PV();
    a->hunger();
    
    CPPUNIT_ASSERT( a->get_PV() == life - 1 );
}

void Be_alive_Test::test_is_dead_false(){
    CPPUNIT_ASSERT( a->is_dead() == false);
}

void Be_alive_Test::test_is_dead_PV_true(){
    while(a->get_PV() > 0){
        a->hunger();
    }

    CPPUNIT_ASSERT(a->is_dead() == true);
}

void Be_alive_Test::test_natural_death_false(){
    CPPUNIT_ASSERT(a->natural_death() == false);
}

void Be_alive_Test::test_natural_death_true(){
    //a->set_PV(this->PV_test);
    //std::cout << a->get_PV() << std::endl;
    CPPUNIT_ASSERT(a->get_PV() == this->PV_test);

    while(a->get_age() <= 21){
        a->aging();
    }

    CPPUNIT_ASSERT(a->natural_death() == true);
}

void Be_alive_Test::test_is_dead_age_true(){
    while(a->get_age() <= 21){
        a->aging();
    }
    
    CPPUNIT_ASSERT(a->is_dead() == true);
}