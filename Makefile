CC = g++
arg = -Wall -std=c++2a

.DEFAULT_GOAL := simulation

bin/aquarium.o: src/aquarium/Aquarium.cpp src/aquarium/Aquarium.h src/aquarium/be_alive/algae/Algue.h src/aquarium/be_alive/fish/Poisson.h
	$(CC) $(arg) -c -o $@ $< 

bin/reproduction.o: src/aquarium/be_alive/Reproduction.cpp src/aquarium/be_alive/Reproduction.h src/aquarium/be_alive/algae/Algue.h
	$(CC) $(arg) -c -o $@ $< 

bin/be_alive.o: src/aquarium/be_alive/Be_alive.cpp src/aquarium/be_alive/Be_alive.h src/aquarium/be_alive/Reproduction.h
	$(CC) $(arg) -c -o $@ $<

bin/algae.o: src/aquarium/be_alive/algae/Algue.cpp src/aquarium/be_alive/algae/Algue.h src/aquarium/be_alive/Be_alive.h
	$(CC) $(arg) -c -o $@ $<

bin/poisson.o: src/aquarium/be_alive/fish/Poisson.cpp src/aquarium/be_alive/fish/Poisson.h src/aquarium/be_alive/Be_alive.h src/aquarium/be_alive/algae/Algue.h
	$(CC) $(arg) -c -o $@ $<

bin/display.o: src/aquarium/display/Display.cpp src/aquarium/display/Display.h src/aquarium/be_alive/fish/Poisson.h src/aquarium/be_alive/algae/Algue.h
	$(CC) $(arg) -c -o $@ $<

bin/main.o: src/main.cpp src/aquarium/Aquarium.cpp src/aquarium/Aquarium.h
	$(CC) $(arg) -c -o $@ $<

simulation: bin/aquarium.o bin/reproduction.o bin/be_alive.o bin/algae.o bin/poisson.o bin/display.o bin/main.o
	$(CC) -g -o simulation bin/aquarium.o bin/reproduction.o bin/be_alive.o bin/algae.o bin/poisson.o bin/display.o bin/main.o

clear: 
	rm -f bin/*.o

clear_All: clear
	rm -f simulation